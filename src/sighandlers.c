#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "sighandlers.h"
#include "server.h"

#define SIGHUP  1   /* Hangup the process */ 
#define SIGINT  2   /* Interrupt the process */
#define SIGQUIT 3   /* Quit the process */ 
#define SIGILL  4   /* Illegal instruction. */ 
#define SIGTRAP 5   /* Trace trap. */ 
#define SIGABRT 6   /* Abort. */

void sigint_handler(int sig);
void sighup_handler(int sig);


void sigint_handler(int sig){
  /* printf("Caught sigint - %d - exiting \n", sig); */
  perror("Caught sigint - exiting");
  delete_unix_socket();
  exit(0);
}


void sighup_handler(int sig){
  /* printf("Caught sighup - %d - exiting \n", sig); */
  perror("Caught sighup - exiting");
  delete_unix_socket();
  exit(0);
}
