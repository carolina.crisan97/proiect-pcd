#ifndef SERVER_H
#define SERVER_H

void admin_handler();
void tcp_handler();
void udp_handler();
void delete_unix_socket();
int main();

#endif
