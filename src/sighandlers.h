#ifndef SIGHANDLERS_H
#define SIGHANDLERS_H

void sigint_handler(int sig);
void sighup_handler(int sig);

#endif
