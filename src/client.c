#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/un.h>
#include <resolv.h>
#include <strings.h>
#include <ncurses.h>
#include <menu.h>
#include <pthread.h>
#include "sighandlers.h"

#define ADDRESS "127.0.0.1"
#define UN_SERVER_SOCKPATH "/tmp/mttrk-admsock"
#define CLIENT_PATH "/tmp/mttrk-clisock"
#define TCP_PORT 1042
#define UDP_PORT 1043
#define MAXLINE 1024

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

char* subp_list[2048];
int nr_clients = 0;
unsigned long last_update = 0;
WINDOW* main_window;

void init_main_window(){
  MENU* subprocesses_menu;
  ITEM** items;
  int width, height;
  int ch;

  while(1){
    if(time(NULL) - last_update > 5000){
      height = (long int) getenv("LINES");
      width = (long int) getenv("COLUMNS");
      initscr();
      cbreak();
      curs_set(0);
      noecho();
      touchwin(main_window);
      refresh();

      if(has_colors() == TRUE){
	start_color();
      }
  
      init_pair(1, COLOR_GREEN, COLOR_BLACK);

      main_window = newwin(height, width, 0, 0);
      keypad(main_window, TRUE);
      box(main_window, 0, 0);
      wrefresh(main_window);
      mvprintw(1, 2, "%s", "Proiect PCD");

      for(int i = 10; i < 32; ++i){
	mvprintw(i, 67, "%s", "|");
      }

      mvprintw(10, 20, "%s", "Clienti conectati:");
      mvprintw(10, 100, "%s", "Subprocese:");
  
      for(int i = 2; i < 134; ++i){
	mvprintw(9, i, "%s", "-");
	mvprintw(11, i, "%s", "-");
      }

      mvprintw(1, 80, "%s", "Status");
      mvprintw(2, 80, "%s", "Numar de clienti:");
      attron(COLOR_PAIR(1));
      mvprintw(2, 99, "%d", nr_clients);
      attroff(COLOR_PAIR(1));
      mvprintw(3, 80, "%s", "Server TCP:");
      mvprintw(4, 80, "%s", "Server UDP:");

      mvprintw(5, 80, "%s", "UNIX domain socket server:");
      attron(COLOR_PAIR(1));
      mvprintw(5, 107, "ON", UN_SERVER_SOCKPATH);
      attroff(COLOR_PAIR(1));

      int n_choices = ARRAY_SIZE(subp_list);
      items = (ITEM **) calloc(n_choices, sizeof(ITEM *));
  
      for(int i = 0; i < n_choices; ++i){
	if(subp_list[i] != 0){
	  items[i] = new_item("-", subp_list[i]);
	}
      }

      subprocesses_menu = new_menu(items);
      set_menu_win(subprocesses_menu, main_window);
      set_menu_sub(subprocesses_menu, derwin(main_window, 22, 60, 12, 68));
      set_menu_mark(subprocesses_menu, " * ");

      post_menu(subprocesses_menu);
      wrefresh(main_window);
      refresh();
    }
    
    else{
      ch = wgetch(main_window);
      switch(ch){
      case 113:
	endwin();
	exit(1);
	break;
      case KEY_DOWN:
	menu_driver(subprocesses_menu, REQ_DOWN_ITEM);
	break;
      case KEY_UP:
	menu_driver(subprocesses_menu, REQ_UP_ITEM);
	break;
      default:
	refresh();
	break;
      }
    }
  }
}


void print_usage(){
  printf("\nUsage: client [client-mode]\n\nClient modes:\n  -U \t Unix socket\n  -t \t TCP\n  -u \t UDP\n");  
}


void unix_client(){
  int client_sock, rc, len;
  char buf[2048], credentials[40];
  struct sockaddr_un server_sockaddr; 
  struct sockaddr_un client_sockaddr;
  
  memset(&server_sockaddr, 0, sizeof(struct sockaddr_un));
  memset(&client_sockaddr, 0, sizeof(struct sockaddr_un));
     
  client_sock = socket(AF_UNIX, SOCK_STREAM, 0);
  if(client_sock == -1){
    printf("SOCKET ERROR\n");
    exit(1);
  }

  client_sockaddr.sun_family = AF_UNIX;   
  strcpy(client_sockaddr.sun_path, CLIENT_PATH);
  server_sockaddr.sun_family = AF_UNIX;
  strcpy(server_sockaddr.sun_path, UN_SERVER_SOCKPATH);
  len = sizeof(client_sockaddr);
  unlink(CLIENT_PATH);
  
  if((rc = bind(client_sock, (struct sockaddr *) &client_sockaddr, len)) == -1){
    perror("Bind error!");
    close(client_sock);
    exit(1);
  }
  
  rc = connect(client_sock, (struct sockaddr *) &server_sockaddr, len);

  int* recv_len = malloc(sizeof(int));
  memset(buf, 0, sizeof(buf));

  read(client_sock, recv_len, sizeof(int));
  read(client_sock, buf, *recv_len);
  
  if(strncmp(buf, "AUTH", 4) == 0){
    printf("Enter admin authentication credentials\n(username:password format) - ");
    memset(credentials, 0, sizeof(credentials));
    fgets(credentials, sizeof(credentials), stdin);
    write(client_sock, credentials, sizeof(credentials));
  }

  memset(buf, 0, strlen(buf));
  read(client_sock, recv_len, sizeof(int));
  read(client_sock, buf, *recv_len);

  if(strcmp(buf, "CONNECTED") == 0){
    int i = 0;
    int* recv_len = malloc(sizeof(int));
    pthread_t curses;

    if(pthread_create(&curses, NULL, init_main_window, NULL)){
      perror("Could not create ncurses window thread.");
    }


    while(read(client_sock, recv_len, sizeof(int))){
      if(*recv_len == 0)
	break;
      memset(buf, 0, strlen(buf));
      read(client_sock, buf, *recv_len);
      subp_list[i] = malloc(*recv_len + 1);
      strcpy(subp_list[i], buf);
      subp_list[i] = strtok(subp_list[i], "\n");
      last_update = time(NULL);
      i++;
    }

    read(client_sock, &nr_clients, sizeof(int));

    while(1){
    }
  }
  else{
    endwin();
    close(client_sock);
  }
}


void tcp_client(){
  int sock = 0;
  struct sockaddr_in serv_addr; 
  
  if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0){ 
    printf("\n Socket creation error \n"); 
  } 

  memset(&serv_addr, '0', sizeof(serv_addr)); 
  serv_addr.sin_family = AF_INET; 
  serv_addr.sin_port = htons(TCP_PORT); 
	
  if(inet_pton(AF_INET, ADDRESS, &serv_addr.sin_addr)<=0){
    printf("\nInvalid address/ Address not supported \n"); 
  } 

  if(connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0){
    printf("\nConnection Failed \n"); 
  }
  
  send(sock, "Salut!!!", strlen("Salut!!!"), 0);
  printf("Mesaj trimis!\n");
}


void udp_client(){
  int sockfd;
  char send_buffer[9068];
  struct sockaddr_in servaddr; 
  FILE* csv = fopen("/home/Sarah/Download/proiect-pcd/src/data/data.csv", "r");
  
  if(csv == NULL){
    printf("Nu pot deschide CSV!\n");
    exit(1);
  }

  if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ){ 
    perror("eroare creare socket"); 
    exit(EXIT_FAILURE); 
  } 

  memset(&servaddr, 0, sizeof(servaddr)); 
  servaddr.sin_family = AF_INET; 
  servaddr.sin_port = htons(UDP_PORT); 
  servaddr.sin_addr.s_addr = inet_addr(ADDRESS); 

  int fs_block;
  memset(&send_buffer, 0, sizeof(send_buffer));
  while((fs_block = fread(send_buffer, sizeof(char), 2048, csv)) > 0){
    sendto(sockfd, send_buffer, sizeof(send_buffer), MSG_CONFIRM, (const struct sockaddr *) &servaddr, sizeof(servaddr));
    memset(&send_buffer, 0, sizeof(send_buffer));
  }
  
  printf("Trimite logul CSV catre server\n");
  close(sockfd);
}


int main(int argc, char *argv[]){
  char option;
  while((option = getopt(argc, argv, "tuUh"))!= -1){
    switch(option){
    case 't':
      tcp_client();
      break;
    case 'u':
      udp_client();
      break;
    case 'U':
      unix_client();
      break;
    case 'h':
      print_usage();
      break;
    case '?':
      print_usage();
      exit(1);
    }
  }
  
  return 0;
}
