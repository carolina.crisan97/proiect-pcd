#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/un.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include "server.h"
#include "sighandlers.h"

#define ADDRESS "127.0.0.1"
#define UN_SERVER_SOCKPATH "/tmp/mttrk-admsock"
#define TCP_PORT 1042
#define UDP_PORT 1043

#define MAX_CONNECTIONS 2048
#define MAXLINE 1024
#define MAXBUF 1024

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
int subproc_nr = 0;
int admin_connected = 0;
int number_clients = 0;

typedef struct status{
  int port_no;
  int up_down;
  char proto[5];
} Status;


void print_usage(){
  printf("\nUsage: server [server-mode]\n\nServer modes:\n  -U \t Unix socket\n  -t \t TCP\n  -u \t UDP\n");
}


void delete_unix_socket(){
  unlink(UN_SERVER_SOCKPATH);
}


const char* getfield(char* line, int num){
    const char* tok;
    for (tok = strtok(line, ";"); tok && *tok; tok = strtok(NULL, ";\n")){
        if (!--num)
            return tok;
    }
    return NULL;
}


char* gen_output_log_name(struct sockaddr_in socket, int port_no, unsigned long timestamp){
  char* address = (char*) inet_ntoa(socket.sin_addr);
  char* port[6];
  char* time[10];
  int i = 0;
  
  while(address[i] != '\0'){
    if(address[i] == '.')
      address[i] = '-';
    ++i;
  }

  sprintf(port, "%d", port_no);
  sprintf(time, "%lu", timestamp);
  strcat(address, "-");
  strcat(address, port);
  strcat(address, "-");
  strcat(address, time);
  return strcat(address, ".csv");
}

char* processes[4096];

void fetch_subprocesses(){
  char proc[1024];
  FILE* fp = popen("/bin/ps -eopid,cmd | grep -v grep | grep ./server | sed 's/^//'", "r");


  while(fgets(proc, sizeof(proc), fp) != 0) {
    processes[subproc_nr] = malloc(strlen(proc) + 1);
    strcpy(processes[subproc_nr], proc);
    ++subproc_nr;
  }
  
  pclose(fp);
}


void fetch_users(){
}


int authenticate_admin(char* credentials){
  char* username = strtok(credentials, ":");
  char* password = strtok(NULL, ":");
  
  if(strncmp(username, "user", 5) == 0 && strncmp(password, "parola", 6) == 0){
    return 1;
  }
  else{
    return 0;
  }
}


void unix_handler(){
  int listen_desc = 0, conn_desc = 0;
  char recvBuff[2048];
  struct sockaddr_un server;

  unlink(UN_SERVER_SOCKPATH);
  
  server.sun_family = AF_UNIX;
  strcpy(server.sun_path, UN_SERVER_SOCKPATH);
  listen_desc = socket(AF_UNIX, SOCK_STREAM, 0);
  bind(listen_desc, (struct sockaddr*) &server, sizeof(server));
  
  listen(listen_desc, 1);
  printf("Unix socket created for admin at: %s\n", UN_SERVER_SOCKPATH);
  

  while(1){
    if(fork() >= 0){
      conn_desc = accept(listen_desc, (struct sockaddr*) NULL, NULL);

      if(conn_desc > 0){
	  printf("An admin connected - %d\n", conn_desc);
	  int* send_len = malloc(sizeof(int));
	  *send_len = 4;

	  memset(recvBuff, 0, sizeof(recvBuff));
	  write(conn_desc, send_len, sizeof(int));
	  write(conn_desc, "AUTH", 4);
	  read(conn_desc, recvBuff, sizeof(recvBuff));

	  if(authenticate_admin(recvBuff) == 1){
	    *send_len = 9;
	    write(conn_desc, send_len, 4);
	    write(conn_desc, "CONNECTED", 9);
	    number_clients++;
	    sleep(1);

	    fetch_subprocesses();
	    for(int i = 0; i < subproc_nr; ++i){
	      *send_len = strlen(processes[i]);
	      write(conn_desc, send_len, sizeof(int));
	      write(conn_desc, processes[i], strlen(processes[i]));
	    }

	    int x = 0;
	    write(conn_desc, &x, sizeof(int));
	    write(conn_desc, &number_clients, sizeof(int));
	    
	    while(read(conn_desc, recvBuff, sizeof(recvBuff - 1)) > 0){
	    }
	  }
	  printf("Client disconnected\n");
      }
    }
    else{
    }
  }
}


void tcp_handler(){
  int listen_desc = 0, conn_desc = 0;
  struct sockaddr_in server;
  char sendBuff[1025], recvBuff[1025];
  time_t ticks;

  listen_desc = socket(AF_INET, SOCK_STREAM, 0);
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  server.sin_port = htons(TCP_PORT);
  
  bind(listen_desc, (struct sockaddr*) &server, sizeof(server));
  listen(listen_desc, MAX_CONNECTIONS);
  printf("Asculta portul TCP: %d\n", TCP_PORT);

  while(1){
    if(fork() >= 0){
      conn_desc = accept(listen_desc, (struct sockaddr*) NULL, NULL);

      if(conn_desc > 0){
	printf("Un client este conectat - %d\n", conn_desc);
	printf(sendBuff, sizeof(sendBuff), "%s\r\n", ctime(&ticks));

	write(conn_desc, "Connected\n", 11);

	while(read(conn_desc, recvBuff, sizeof(recvBuff - 1)) > 0){
	  printf("%s\n", recvBuff);
	  fflush(stdout);
	  
	  if(strncmp(recvBuff, "exit", 4) == 0){
	    exit(1);
	  }
	}

	sleep(1);
	printf("Client deconectat\n");
      }
    }
    else{
    }
  }
}


void udp_handler(){
  int sd;
  socklen_t len_client;
  ssize_t rc;
  char buffer[MAXBUF];
  struct sockaddr_in server_addr;
  struct sockaddr_in client_addr;
  FILE* output;

  memset(&server_addr, 0, sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(UDP_PORT);
  server_addr.sin_addr.s_addr = INADDR_ANY;
  sd = socket (AF_INET,SOCK_DGRAM,0);
  
  if(bind(sd, (struct sockaddr*) &server_addr, sizeof(server_addr)) != 0){
      perror("Bind error");
      exit(0);
  }

  printf("UDP server listening on port: %d\n", ntohs(server_addr.sin_port));
  len_client = sizeof(client_addr);
  memset(&client_addr, 0, sizeof(client_addr));

  if(fork() >= 0){
    int opened_flag = 0;

    while((rc = recvfrom(sd, buffer, 2048, MSG_WAITALL, (struct sockaddr*) &client_addr, &len_client)) > 0){
      if(opened_flag == 0){
	opened_flag = 1;
	output = fopen(gen_output_log_name(client_addr, server_addr.sin_port, time(NULL)), "w");
	dup2(fileno(output), STDOUT_FILENO);
      }

      printf(buffer);
      memset(&buffer, 0, sizeof(buffer));
      fflush(stdout);
    }
    
    fclose(output);
  }
  else{
  }
}


int main(int argc, char* argv[]){
  signal(SIGINT, sigint_handler);
  
  char option;
  while((option = getopt(argc, argv, "tuUh"))!= -1){
    switch(option){
    case 't':
      tcp_handler();
      break;
    case 'u':
      udp_handler();
      break;
    case 'U':
      unix_handler();
      break;
    case 'h':
      print_usage();
      break;
    case '?':
      print_usage();
      exit(1);
    }
  }

  return 0;
}
