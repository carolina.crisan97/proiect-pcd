import socket

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(('127.0.0.1', 1042))
output = "I'm a connected client."
client.sendall(output.encode('utf-8'))
from_server = client.recv(4096)
client.close()

print(from_server)
